Building the website has the following prereqs

NodeJS
http://nodejs.org/

Grunt
From command type
npm install -g grunt-cli

Then from the project directory type

npm install -d

This will install all of the dependencies for the grunt tasks.

Then the following two grunt tasks will get you up and running

grunt build
grunt

You will then be able to preview the website at

http://localhost:8001/

this site uses php for database interation so to preview with working data you will need to run through a local server like WAMP

copy contents of the public folder after building to the www folder in your WAMP directory on C: 
(yes I'm using windows)

