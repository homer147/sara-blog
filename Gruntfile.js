module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    stylus: {
    	compile: {
    		files: {'public/css/style.css' : 'app/stylus/style.styl', 
        'public/admin/dashboard/css/admin-style.css' : 'app/stylus/admin/dashboard/admin-style.styl'}
    	} 
    },

    // concat: {
    //   js : {
    //     src : ['app/js/*'],
    //     dest : 'app/js/combined/script.js'
    //   }
    // },

    // uglify : { 
    //   js: {
    //     files: {
    //       'public/js/script.min.js' : [ 'app/js/combined/script.js' ]
    //     }
    //   }
    // },

    copy: {
      main: {
      	files: [{expand: true, cwd: 'app/assets', dot: true, src: ['**'], dest: 'public/'},
      	{expand: true, cwd: 'app/html', dot: true, src: ['**'], dest: 'public/'}]
      }
    },

    watch: {
      options: {
      	livereload: true
      },
      assets: {
      	files: ['app/assets/**/*.*'],
      	tasks: ['newer:copy'],
      	options: {
      		spawn: false
      	}
      },
      html: {
      	files: ['app/html/**/*.*'],
      	tasks: ['newer:copy'],
      	options: {
      		spawn: false
      	}
      },
      // js: {
      // 	files: ['app/js/**/*.js'],
      //   tasks: ['concat', 'uglify'],
      //   options: {
      //   	spawn: false
      //   }
      // },      
      css: {
      	files: ['app/stylus/**/*.styl'],
      	tasks: ['stylus'],
      	options: {
      		spawn: false
      	}
      }
    },

    connect: {
      server: {
    	options: {
    	  port: 8001,
    	  base: 'public'
    	}
      }
    }

  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks("grunt-contrib-stylus");
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks("grunt-newer");
  
  grunt.registerTask('build', ['copy', /*'concat', 'uglify', */'stylus']);
  grunt.registerTask('default', ['connect', 'watch']);

};