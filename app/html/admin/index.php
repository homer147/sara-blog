<?php
include_once '../auth/db_connect.php';
include_once '../auth/functions.php';

sec_session_start();

if(login_check($mysqli) == true) {
	$logged = 'in';
} else {
	$logged = 'out';
}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Sara Blog Admin</title>

	<link rel="stylesheet" href="css/style.css">

	<script type="text/JavaScript" src="js/sha512.js"></script> 
    <script type="text/JavaScript" src="js/forms.js"></script>

</head>
<body>

	<?php
	if(isset($_GET['error'])) {
		echo '<p class="error">Error logging in.</p>';
	}
	?>

	<form action="../auth/process_login.php" method="post" name="login_form">

		Email: <input type="email" name="email" />
		Password: <input type="password" name="password" id="password" />
		<input type="button" value="Login" onclick="formhash(this.form, this.form.password);" />

	</form>

	<?php
	if(login_check($mysqli) == true) {
		echo '<p>Currently logged ' . $logged . ' as ' . htmlentities($_SESSION['username']) . '.</p>';
		echo '<p>Logout <a href="../auth/logout.php">Log out</a>.</p>';
	} else {
		echo '<p>Currently logged ' . $logged . '.</p>';
	}
	?>

</body>
</html>