app.directive('postSummary', function() {
	return {
		restrict: 'E',
	    templateUrl: 'partials/_post-summary.html',
		controller: 'displayPostCtrl',
	    scope: {
	    	postOrder: '@'
	    }
	};
});

app.directive('blogPostSummary', function() {
	return {
		restrict: 'E',
	    templateUrl: 'partials/_blog-post-summary.html',
	    controller: 'blogPostCtrl'
	};
});