var app = angular.module('myApp', ['ui.router']);

app.config(function($stateProvider, $urlRouterProvider, $locationProvider){
	$urlRouterProvider.otherwise('/');
	$stateProvider
		.state('home', {
			url: '/',
			templateUrl: 'partials/_home.html'
		})
		//.state('home.paragraph', {
			//url: 'paragraph',
			//template: 'lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum '
		//})
		.state('blog', {
			url: '/blog',
			templateUrl: 'partials/_blog.html'
		})
		.state('about', {
			url: '/about',
			templateUrl: 'partials/_about.html'
		})
		.state('connect', {
			url: '/connect',
			templateUrl: 'partials/_connect.html',
			controller: 'contactForm'
		});

		$locationProvider.html5Mode({
			enabled: 'true',
			requireBase: 'false'
		});
})