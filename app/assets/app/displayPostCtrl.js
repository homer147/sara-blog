app.controller('displayPostCtrl', function($scope, Data) {
	$scope.blogPost = {};
	var url = 'blogPosts/' + $scope.postOrder;
	Data.get(url).then(function(data) {
		$scope.blogPosts = data.data;
	});
});

app.controller('blogPostCtrl', function($scope, Data) {
	$scope.blogPost = {};
	Data.get('blogPosts').then(function(data) {
		$scope.blogPosts = data.data;
	});
});

app.controller('contactForm', function($scope, $http) {
	$scope.result = 'hidden';
	$scope.resultMessage;
	$scope.formData;
	$scope.submitButtonDisabled = false;
	$scope.submitted = false;

	$scope.submit = function(contactFormInput) {
		$scope.submitted = true;
		$scope.submitButtonDisabled = true;

		if(contactFormInput.$valid) {
			$scope.submitButtonDisabled = true;
			$http({
				method: 'POST',
				url: '/contact/process-form.php',
				data: angular.element.param($scope.formData),
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).success(function(data) {
				console.log(data);
				if(data.type == 'success') {
					$scope.submitButtonDisabled = false;
					$scope.resultMessage = data.message;
					$scope.result = 'bg-success';
					$scope.formData = {};
				} else {
					$scope.submitButtonDisabled = false;
					$scope.resultMessage = data.message;
					$scope.result = 'bg-danger';
				}

			})
		} else {
			//console.log('not valid');
			$scope.submitButtonDisabled = false;
			$scope.resultMessage = 'failed';
			$scope.result = 'bg-danger';
		}		
	}
});