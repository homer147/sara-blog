<?php

require '.././libs/Slim/Slim.php';
require_once 'dbHelper.php';

\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim();
$db = new dbHelper();

/**
 * Database Helper Function templates
 */
/*
select(table name, where clause as associative array)
insert(table name, data as associative array, mandatory column names as array)
update(table name, column names as associative array, where clause as associative array, required columns as array)
delete(table name, where clause as array)
*/

// Blog Posts
$app->get('/blogPosts', function() {
	global $db;
	$rows = $db->select("tbl_post", "id,title,content,status", "1=1", array());
	echoResponse(200, $rows);
});

$app->get('/blogPosts/:id', function($id) {
	global $db;
	$rows = $db->select("tbl_post", "id,title,content,status", "id=$id", array());
	echoResponse(200, $rows);
});

/*

$app->post('/blogPosts', function() use ($app) {
	$data = json_decode($app->request->getBody());
	$mandatory = array('name');
	global $db;
	$rows = $db->insert("tbl_post", $data, $mandatory);
	if($rows["status"] == "success")
		$rows["message"] = "Post added successfully";
	echoResponse(200, $rows);
});

$app->put('/blogPosts/:id', function($id) use ($app) {
	$data = json_decode($app->request->getBody());
	$condition = array('id'=>$id);
	$mandatory = array();
	global $db;
	$rows = $db->update("tbl_post", $data, $condition, $mandatory);
	if($rows["status"] == "success")
		$rows["message"] = "Post updated successfully";
	echoResponse(200, $rows);
});

$app->delete('/blogPosts/:id', function($id) {
	global $db;
	$rows = $db->delete("tbl_post", array('id'=>$id));
	if($rows["status"] == "success")
		$rows["message"] = "Post removed successfully";
	echoResponse(200, $rows);
});

*/

function echoResponse($status_code, $response) {
	global $db;
	$app = \Slim\Slim::getInstance();
	$app->status($status_code);
	$app->contentType('application/json');
	echo json_encode($response);
}

$app->run();