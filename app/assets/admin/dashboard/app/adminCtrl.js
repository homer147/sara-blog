app.controller('postAdmin', function($scope, Data) {
	$scope.blogPosts = {};
	Data.get('blogPosts').then(function(data) {
		$scope.blogPosts = data.data;
		$scope.blogPosts.reverse();
	});

	$scope.editLink = function(id) {
			window.location = '/admin/editpost?post_id=' + id;
	};
});

app.controller('editPost', function($scope, Data) {

	$scope.blogPost = {};
	$scope.postId = $scope.getUrlValue('post_id');

	var url = 'blogPosts/' + $scope.postId;
	Data.get(url).then(function(data) {
		$scope.blogPost = data.data;
	});

	$scope.submit = function(editPostFormInput) {

	}
});