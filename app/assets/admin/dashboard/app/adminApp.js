var app = angular.module('adminApp', ['ui.router']);

app.config(function($stateProvider, $urlRouterProvider, $locationProvider) {
	$urlRouterProvider.otherwise('/admin/dashboard');
	$stateProvider
		.state('homeAdmin', {
			url: '/admin/dashboard',
			templateUrl: 'dashboard/partials/_home-admin.html'
		})
		.state('postAdmin', {
			url: '/admin/blogposts',
			templateUrl: 'dashboard/partials/_post-admin.html',
			controller: 'postAdmin'
		})
		.state('editPost', {
			url: '/admin/editpost',
			templateUrl: 'dashboard/partials/_post-edit.html',
			controller: 'editPost'
		});

		$locationProvider.html5Mode({
			enabled: 'true',
			requireBase: 'false'
		});
});

app.run(function($rootScope) {
	$rootScope.getUrlValue = function(varSearch) {
		var searchString = window.location.search.substring(1);
		var variableArray = searchString.split('&');
		for(var i = 0; i < variableArray.length; i++) {
			var keyValuePair = variableArray[i].split('=');
			if(keyValuePair[0] == varSearch) {
				return keyValuePair[1];
			}
		}
	};
})