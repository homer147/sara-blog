<?php
	
	$to_Email = "michael.f.browning@gmail.com";
	$subject = 'sara-blog visitor';	

    $sender_Name = check_input($_POST["name"],"Please enter your name");
    $sender_Email = htmlspecialchars($_POST["email"]);
    $sender_Message = check_input($_POST["message"],"Please enter your message");
    $sender_Comment = $_POST["comment"];

    if (!filter_var($sender_Email, FILTER_VALIDATE_EMAIL))
	{
		$output = json_encode(array('type'=>'error', 'message' => 'Please enter a valid email'));
	    die($output);
	}

    function check_input($data, $problem='')
	{
	    $data = trim($data);
	    $data = htmlspecialchars($data);
	    if ($problem && strlen($data) == 0)
	    {
	    	$output = json_encode(array('type'=>'error', 'message' => $problem));
	        die($output);
	    }
	    return $data;
	}

    $headers = 'From: ' . $sender_Email . "\r\n" . 'Reply-To: ' . $sender_Email . "\r\n";

    if (empty($sender_Comment))
    {
		$sentMail = mail($to_Email, $subject, $sender_Message.' - '.$sender_Name, $headers);

	    if(!$sentMail)
	    {
	        $output = json_encode(array('type'=>'error', 'message' => 'There was a problem, your message was not sent. Please try again.'));
		    die($output);
	    }else{
	    	$output = json_encode(array('type'=>'success', 'message' => 'Thank you. Your message has been sent.'));
		    die($output);
	    }
	} else {
		$output = json_encode(array('type'=>'error', 'message' => 'There was a problem, your message was not sent. Please try again.'));
		die($output);
	}
	 
?>