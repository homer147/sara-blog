<?php
// Database configuration
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_HOST', 'localhost');
define('DB_NAME', 'users');

define('CAN_REGISTER', 'any');
define('DEFAULT_ROLE', 'member');

define('SECURE', FALSE);

?>